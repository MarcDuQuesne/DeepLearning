![Pylint Score][score]
![pipeline status][status]


#### Deep Learning

This is just a collection of tools and educational projects with a common theme, for my own amusement. The collection grows as I learn things.

#### Tools used

- python3.6
- keras [???]
- tensorflow

Optionally:

- imagedb utils (see docs)


Status


[score]:   https://gitlab.com/MarcDuQuesne/DeepLearning/-/jobs/artifacts/master/raw/public/pylint.svg?job=pylint "Pylint Score"
[status]:  https://gitlab.com/MarcDuQuesne/DeepLearning/badges/master/pipeline.svg "Status"
[commits]: https://gitlab.com/MarcDuQuesne/DeepLearning/commits/master