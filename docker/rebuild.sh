#!/bin/bash

# create/modify the Dockerfile, then

if [[ ! -f Dockerfile ]] 
then
    echo 'No Dockerfile detected, exiting...'
    exit 1
fi

docker login registry.gitlab.com
docker build -t registry.gitlab.com/marcduquesne/deeplearning .
docker push registry.gitlab.com/marcduquesne/deeplearning
