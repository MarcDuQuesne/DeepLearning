import time
import logging
import cv2

from Utils.Animation.draw import create_blank
from Utils.Animation.hud import Hud

from Utils.OutputStream import OutputStream
from Utils.WebcamVideoStream import WebcamVideoStream
from Utils.Process import Process_Image

from Utils.Config import Config

TIMESTEP = 0.1
height, width = 480,640

white=(255,255,255)

# set up stuff
logging.basicConfig(level=logging.INFO, format='[%(asctime)6s][%(threadName)10s][%(levelname)6s] %(message)s')

def main():
  
  logging.info('Starting main program.')

  config = Config('main.cfg')

  # Input thread
  ws = WebcamVideoStream()
  ws.start()
  ok,img = ws.read()

  # Output thread.
  vs = OutputStream(config)
  vs.initialize(img.shape)
  vs.start()

  # Process thread
  worker = Process_Image(vs,config)
  worker.start()

  run(config,ws,worker,vs)

def run(config,ws,worker,vs):
  # -------------------- 
  # main loop
  counter=0

  logging.info('Starting main loop.')
  while True:

    if counter == 50:
      break

    # 1) acquire image from stream.
    ok,img = ws.read(skip=5)
    if not ok:
      continue

    counter+=1
  
    # 2) process it
    worker.q.put(img)
    
    # 3) put image in the output queue and handle user input.
    if config.output.screen:
    
      key = vs.key

      #if the `q` key is pressed, break from the loop
      if key == ord("q"):
        break
      if key == ord("s"):
        ws.hud.RM.toggle()
      if key == ord("s"):
        ws.hud.RM.toggle()

    # elapsed_time = time.time() - start_time
    # time.sleep(TIMESTEP)

    # tot_time = time.time() - start_time
    #print(tot_time)
  
  # end of main loop

  while not worker.q.empty():
    time.sleep(1)
  
  vs.stop()
  ws.stop()
  worker.stop()
  
  cv2.destroyAllWindows()

  logging.info('Main Thread, all done.')
  
if __name__ == "__main__":
  main()
