#!/usr/bin/env python3.6

# global packages. if input is video, more imports are handled in the parse_options.

import logging
import numpy as np
import argparse
import cv2
import imutils
import time

from lib.FileVideoStream import FileVideoStream
from lib.WebcamVideoStream import WebcamVideoStream
from lib.FPS import FPS

# initialize the list of class labels MobileNet SSD was trained to
# detect, then generate a set of bounding box colors for each class
CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
            "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
            "dog", "horse", "motorbike", "person", "pottedplant", "sheep",
            "sofa", "train", "tvmonitor"]
COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))

# only recognize objects once in this interval.
skip = 5 


def parse_options():
  # construct the argument parse and parse the arguments
  ap = argparse.ArgumentParser()
  ap.add_argument("-i", "--input", required=True,
          help="path to input image, video or webcam")
  ap.add_argument("-p", "--prototxt", required=True,
          help="path to Caffe 'deploy' prototxt file")
  ap.add_argument("-m", "--model", required=True,
          help="path to Caffe pre-trained model")
  ap.add_argument("-c", "--confidence", type=float, default=0.6,
          help="minimum probability to filter weak detections")  
  args = vars(ap.parse_args())

  if args['input'] == '0':
    args['input'] = 0

  return args

def load_model(args):

  # load our serialized model from disk
  logging.info('Loading model.')
  return cv2.dnn.readNetFromCaffe(args["prototxt"], args["model"])

def initialize(args):
  if args['input_is_video']:
    # initialize the video stream, allow the cammera sensor to warmup,
    # and initialize the FPS counter
    logging.info('Starting video stream.')

    src = args['input']

    vs = cv2.VideoCapture(src)

    # these two work in a different thread, thus eliminating
    # latency problems and resulting in quite an impressive FPS increase.
    #vs = WebcamVideoStream(src)
    #vs = FileVideoStream(src).start()
    #vs.start()

    time.sleep(2.0)
    fps = FPS().start()

    return vs,fps

def detect():

  # load the input image and construct an input blob for the image
  # by resizing to a fixed 300x300 pixels and then normalizing it
  # (note: normalization is done via the authors of the MobileNet SSD
  # implementation)
  (h, w) = image.shape[:2]
  blob = cv2.dnn.blobFromImage(cv2.resize(image, (300, 300)), 0.007843, (300, 300), 127.5)
  
  # pass the blob through the network and obtain the detections and
  # predictions
  logging.debug('['+str(counter)+']: computing object detections.')
  net.setInput(blob)
  detections = net.forward()
  
  # loop over the detections
  for i in np.arange(0, detections.shape[2]):
    # extract the confidence (i.e., probability) associated with the
    # prediction
    confidence = detections[0, 0, i, 2]
    # filter out weak detections by ensuring the `confidence` is
    # greater than the minimum confidence
    if confidence > args["confidence"]:
      # extract the index of the class label from the `detections`,
      # then compute the (x, y)-coordinates of the bounding box for
      # the object
      idx = int(detections[0, 0, i, 1])
      box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
      (startX, startY, endX, endY) = box.astype("int")
      #display the prediction
      label = "{}: {:.2f}%".format(CLASSES[idx], confidence * 100)
      logging.info('['+str(counter)+']: ' + label)
      cv2.rectangle(image, (startX, startY), (endX, endY),COLORS[idx], 2)
      y = startY - 15 if startY - 15 > 15 else startY + 15
      cv2.putText(image, label, (startX, y),cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[idx], 2


def run(net,args):
  
  vs,fps = initialize(args)

  #fourcc = cv2.VideoWriter_fourcc(*'XVID')
  #out = cv2.VideoWriter('output.avi',fourcc, 20.0, (640,480))

  #while vs.isOpened():
  #for image in frames(args,vs):
  
  counter=0
  #while vs.more():
  while True:

    ok,image = vs.read()
    if not ok:
      break

    counter+=1
    # update the FPS counter
    fps.update()

    if bool(counter%skip) or image is None:
      logging.debug('[' +str(counter)+']: skipping.')
      continue

    image = imutils.resize(image, width=800)

    # load the input image and construct an input blob for the image
    # by resizing to a fixed 300x300 pixels and then normalizing it
    # (note: normalization is done via the authors of the MobileNet SSD
    # implementation)
    (h, w) = image.shape[:2]
    blob = cv2.dnn.blobFromImage(cv2.resize(image, (300, 300)), 0.007843, (300, 300), 127.5)
    
    # pass the blob through the network and obtain the detections and
    # predictions
    logging.debug('['+str(counter)+']: computing object detections.')
    net.setInput(blob)
    detections = net.forward()
    
    # loop over the detections
    for i in np.arange(0, detections.shape[2]):
      # extract the confidence (i.e., probability) associated with the
      # prediction
      confidence = detections[0, 0, i, 2]
      # filter out weak detections by ensuring the `confidence` is
      # greater than the minimum confidence
      if confidence > args["confidence"]:
        # extract the index of the class label from the `detections`,
        # then compute the (x, y)-coordinates of the bounding box for
        # the object
        idx = int(detections[0, 0, i, 1])
        box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
        (startX, startY, endX, endY) = box.astype("int")
        #display the prediction
        label = "{}: {:.2f}%".format(CLASSES[idx], confidence * 100)
        logging.info('['+str(counter)+']: ' + label)
        cv2.rectangle(image, (startX, startY), (endX, endY),COLORS[idx], 2)
        y = startY - 15 if startY - 15 > 15 else startY + 15
        cv2.putText(image, label, (startX, y),cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[idx], 2)

    #out.write(image)
    
    # show the output frame
    cv2.imshow("Frame", image)
    key = cv2.waitKey(1) & 0xFF
 
    # if the `q` key was pressed, break from the loop
    if key == ord("q"):
      break
 

  cv2.destroyAllWindows()
  #vs.stop()

  #out.release()
  #vs.release()
  fps.stop()

  logging.info('')
  logging.info('Elasped time: ' + str(fps.elapsed()))
  logging.info('Approx. FPS: '  + str(fps.fps()))

def main():
  
  logging.basicConfig(level=logging.INFO)
  args = parse_options()

  # TODO how to detect this?
  args.update({'input_is_video' : True})

  if args['input_is_video']:
    logging.info('Loading additional packages.')
    # import the necessary packages
    from imutils.video import VideoStream
    from imutils.video import FPS
    import imutils
    import time

  net = load_model(args)
  run(net,args)

if __name__ == '__main__':
  main()
