#!/usr/bin/env python3.5

from keras.applications import VGG16
from keras import models
from keras import layers
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
from keras.models import load_model

# from keras_fcn.blocks import 

import sys
import os
import numpy as np
import csv
import logging
import shutil
from PIL import Image

logging.basicConfig(level=logging.INFO)

from util import plot_acc
from util import BraceMessage as __

N_OBJECT_TO_RECOGNIZE=3

def add_new_last_layer(conv_base):
  model = models.Sequential()
  model.add(conv_base)
  model.add(layers.Flatten())
  model.add(layers.Dense(1024, activation='relu'))
  model.add(layers.Dropout(0.5))
  model.add(layers.Dense(N_OBJECT_TO_RECOGNIZE, activation='sigmoid'))
  return model

# imagenet DB, containg e.g. n07820145/  n12860365/ obtained through downloadutils.py from imagenet_utils
imagenet_DB = '/home/giani/NonWorkStuff/deep_learning/ImageNet_Utils/DB'

# file containing the wnid -> human readable labels http://image-net.org/archive/words.txt
words_file = '/home/giani/NonWorkStuff/deep_learning/ImageNet_Utils/words.txt'
with open(words_file, mode='r') as infile:
   reader = csv.reader(infile,delimiter='\t',quotechar='\"')
   words  = {rows[0]:rows[1] for rows in reader}

# list of wnids in the current dataset, e.g. n07820145/  n12860365/
# wnids  = [o for o in os.listdir(imagenet_DB) if os.path.isdir(os.path.join(imagenet_DB,o))]
# NB I temporarely override this manually
wnids  = ['n03079136','n00007846','n02834778']
images = {wnid : [name for name in os.listdir(os.path.join(imagenet_DB,wnid,wnid+'_urlimages')) if os.path.isfile(os.path.join(imagenet_DB,wnid,wnid+'_urlimages',name))] for wnid in wnids }

logging.info('Found the following datasets:')
for wnid in wnids:
  logging.info(wnid+' ('+words[wnid]+'): ' + str(len(images[wnid])))

# work directories
dataset_dir = './dataset/' 
train_dir      = os.path.join(dataset_dir,'train')
validation_dir = os.path.join(dataset_dir,'validation')
test_dir       = os.path.join(dataset_dir,'test')

# create directory structure
os.makedirs(dataset_dir,    exist_ok=True)
os.makedirs(train_dir,      exist_ok=True)
os.makedirs(test_dir,       exist_ok=True)
os.makedirs(validation_dir, exist_ok=True)

# splitting of the dataset:
train_p      = 0.64 # 64 % train
validation_p = 0.8  # 16 % validation
test_p       = 1.0  # 20 % test

logging.info(__('Dataset split: train {0:.0f}% validation {1:.0f}% test {2:.0f}%',train_p*100, (validation_p-train_p)*100,(test_p-validation_p)*100))

reload_data = True

if reload_data:

  def copy_if_valid(src,dst):
    try:
      img=Image.open(src)
      img.verify()
      shutil.copyfile(src,dst)
      img.close()
    except (IOError,SyntaxError):
      logging.error(images[wnid][i] + 'is not a valid image.')  

  for wnid in wnids:
    
    logging.info('Preparing dataset for ' + wnid)

    dirname = os.path.join(train_dir,wnid)
    if not os.path.isdir(dirname):
      os.makedirs(dirname,       exist_ok=True)
      for i in range(0, int(len(images[wnid])* train_p) ):
        src=os.path.join(imagenet_DB, wnid,wnid+'_urlimages',images[wnid][i])
        dst=os.path.join(train_dir,wnid,images[wnid][i])
        copy_if_valid(src,dst)  

    dirname = os.path.join(validation_dir,wnid)
    if not os.path.isdir(dirname):
      os.makedirs(dirname, exist_ok=True)
      for i in range(int(len(images[wnid]) * train_p),int(len(images[wnid])*validation_p)):
          src=os.path.join(imagenet_DB, wnid,wnid+'_urlimages',images[wnid][i])
          dst=os.path.join(validation_dir,wnid,images[wnid][i])
          copy_if_valid(src,dst)  
    
    dirname = os.path.join(test_dir,wnid)
    if not os.path.isdir(dirname):
      os.makedirs(dirname, exist_ok=True)
      for i in range(int(len(images[wnid]) * validation_p),int(len(images[wnid])*test_p)):
          src=os.path.join(imagenet_DB, wnid,wnid+'_urlimages',images[wnid][i])
          dst=os.path.join(test_dir,wnid,images[wnid][i])
          copy_if_valid(src,dst)  
  
# Change the batchsize according to your system RAM
train_batchsize = 100
val_batchsize = 10

# use the ImageDataGenerator class to load the images and
# flow_from_directory function to generate batches of images and labels.
train_datagen = ImageDataGenerator(
    #preprocessing_function=preprocess_input #  zero-centers our image data using the mean channel values from the training dataset
    rescale=1./255
    ,rotation_range=90
    ,width_shift_range=0.2, height_shift_range=0.2
    ,horizontal_flip=True, vertical_flip=True
    #,zca_whitening=True # better highlight the structures and features in the image to the learning algorithm.
    ,fill_mode='nearest'
    )

# train_datagen.fit(train_data) #only needed if zca_whitening
train_generator = train_datagen.flow_from_directory(
    #save_to_dir='augmented_dataset/train/', save_prefix='aug', save_format='png',
    directory=train_dir,
    target_size=(size,size),
    batch_size=train_batchsize,
    class_mode='categorical',
    shuffle=True)
    
# Note that the validation data should not be augmented!
validation_datagen = ImageDataGenerator(
  #preprocessing_function=preprocess_input,
  rescale=1./255)

validation_generator = validation_datagen.flow_from_directory(
    validation_dir,
    target_size=(size, size),
    batch_size=val_batchsize,
    shuffle=True,
    class_mode='categorical')

test_datagen = ImageDataGenerator(
  #preprocessing_function=preprocess_input,
  rescale=1./255)

test_generator = test_datagen.flow_from_directory(
    test_dir,
    target_size=(size, size),
    class_mode='categorical',
)

retrain = True
if retrain:

  # load the VGG Model along with the ImageNet weights
  # except the last two fully connected layers
  # which act as the classifier.
  # The last layer has a shape of 7 x 7 x 512.
  conv_base = VGG16(weights='imagenet',
                    include_top=False,
                    input_shape=(size, size, 3))

  conv_base.trainable = False

  fine_tuning=True
  if fine_tuning:
    
    # NOT SURE WTF I was doing here.

    #conv_base.trainable = True
    #set_trainable = False
    #for layer in conv_base.layers:
    #  if layer.name == 'block5_conv1':
    #    set_trainable = True
    # if set_trainable:
    #    layer.trainable = True
    #  else:
    #    layer.trainable = False

    # Freeze the layers except the last 4 layers
    for layer in vgg_conv.layers[:-4]:
      layer.trainable = False
    # Check the trainable status of the individual layers
    #for layer in vgg_conv.layers:
    #  print(layer, layer.trainable))]


  model = add_new_last_layer(conv_base)

from_scratch = False
if from_scratch:

  model = models.Sequential()
  model.add(layers.Conv2D(32, (3, 3), activation='relu',
  input_shape=(150, 150, 3)))
  model.add(layers.MaxPooling2D((2, 2)))
  model.add(layers.Conv2D(64, (3, 3), activation='relu'))
  model.add(layers.MaxPooling2D((2, 2)))
  model.add(layers.Conv2D(128, (3, 3), activation='relu'))
  model.add(layers.MaxPooling2D((2, 2)))
  model.add(layers.Conv2D(128, (3, 3), activation='relu'))
  model.add(layers.MaxPooling2D((2, 2)))
  model.add(layers.Flatten())
  model.add(layers.Dropout(0.5))
  model.add(layers.Dense(512, activation='relu'))
  model.add(layers.Dense(1, activation='sigmoid'))

#model.summary()

model.compile(
    #loss='binary_crossentropy', # binary classificator
   loss='categorical_crossentropy',
   optimizer=optimizers.RMSprop(lr=1e-4),
    #optimizer=adam,
   metrics=['acc'])

history = model.fit_generator(
    generator=train_generator,
    steps_per_epoch=train_generator.samples/train_generator.batch_size,
    epochs=30,
    validation_data=validation_generator,
    validation_steps=validation_generator.samples/validation_generator.batch_size,
    verbose=1)
model.save('traffic.h5')
plot_acc(history)

#model = load_model('plants.h5')
#model = load_model('plants.h5')

model.predict()

logging.info('Evaluating on test set')
scores = model.evaluate_generator(test_generator,100)
logging.info("FINAL Accuracy = " + str(scores[1]))
