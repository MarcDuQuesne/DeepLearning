#!/usr/bin/env python3.6
import cv2
# global packages. if input is video, more imports are handled in the parse_options.
(major_ver, minor_ver, subminor_ver) = (cv2.__version__).split('.')
if int(minor_ver) < 3:
    print('Requires opencv >= 3.4 [opencv-contrib-python]')
    # check also that the trakers exist

import logging
import numpy as np
import argparse
import imutils
import time

from lib.FileVideoStream import FileVideoStream
from lib.WebcamVideoStream import WebcamVideoStream
from lib.FPS import FPS

# Set up tracker.
# BOOSTING    70 FPS
# MIL         15 FPS
# KCF        170 FPS
# TDL          6 FPS
# MEDIANFLOW 140 FPS
# GOTURN     does not work.
tracker_types = ['BOOSTING', 'MIL','KCF', 'TLD', 'MEDIANFLOW', 'GOTURN']
tracker_type = tracker_types[5]

# initialize the list of class labels MobileNet SSD was trained to
# detect, then generate a set of bounding box colors for each class
CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
            "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
            "dog", "horse", "motorbike", "person", "pottedplant", "sheep",
            "sofa", "train", "tvmonitor"]
COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))

# tracking/detect frame image ratio
track_to_detect = 5000

def parse_options():
  # construct the argument parse and parse the arguments
  ap = argparse.ArgumentParser()
  ap.add_argument("-i", "--input", required=True,
          help="path to input image, video or webcam")
  ap.add_argument("-p", "--prototxt", required=True,
          help="path to Caffe 'deploy' prototxt file")
  ap.add_argument("-m", "--model", required=True,
          help="path to Caffe pre-trained model")
  ap.add_argument("-c", "--confidence", type=float, default=0.6,
          help="minimum probability to filter weak detections")  
  args = vars(ap.parse_args())

  if args['input'] == '0':
    args['input'] = 0

  return args

def load_model(args):

  # load our serialized model from disk
  logging.info('Loading model.')
  return cv2.dnn.readNetFromCaffe(args["prototxt"], args["model"])

def initialize(args):
  if args['input_is_video']:
    # initialize the video stream, allow the cammera sensor to warmup,
    # and initialize the FPS counter
    logging.info('Starting video stream.')

    src = args['input']

    #vs = cv2.VideoCapture(src)

    # these two work in a different thread, thus eliminating
    # latency problems and resulting in quite an impressive FPS increase.
    #vs = WebcamVideoStream(src)
    vs = FileVideoStream(src).start()
    #vs.start()

    time.sleep(2.0)

  tracker_type = tracker_types[2]
  if tracker_type == 'BOOSTING':
      tracker = cv2.TrackerBoosting_create()
  if tracker_type == 'MIL':
      tracker = cv2.TrackerMIL_create()
  if tracker_type == 'KCF':
      tracker = cv2.TrackerKCF_create()
  if tracker_type == 'TLD':
      tracker = cv2.TrackerTLD_create()
  if tracker_type == 'MEDIANFLOW':
      tracker = cv2.TrackerMedianFlow_create()
  if tracker_type == 'GOTURN':
      tracker = cv2.TrackerGOTURN_create()

  fps = FPS().start()

  # output staff
  #fourcc = cv2.VideoWriter_fourcc(*'XVID')
  #out = cv2.VideoWriter('output.avi',fourcc, 20.0, (640,480))

  #while vs.isOpened():
  #for image in frames(args,vs):

  return vs,fps,tracker

def run(net,args):
  
  vs,fps,tracker = initialize(args)

  # counter for frames
  counter=0
  tracker_ok = False
  bbbox = []
  label = 'None'
  idx = 0
  bbox = None


  #while vs.more():
  while True:
    
    # update the FPS counter and the frame counter
    fps.update()
    counter+=1

    vs_ok, image = vs.read()
    if not vs_ok:
      break
    image = imutils.resize(image, width=800)

    if image is None:
      logging.debug('[' +str(counter)+']: skipping.')
      continue

    # Update the tracking result
    if bool(counter%track_to_detect) and tracker_ok:
      tracker_ok, bbox = tracker.update(image)
      bbox = np.array(bbox).astype('int')
      box = (bbox[0],bbox[1], bbox[0] + bbox[2], bbox[1] + bbox[3])
      logging.info('['+str(counter)+']: tracking object.'+str(tracker_ok))      

    # if the tracker lost the object, or if enough time has passed, use detection.
    if counter == 1 or not bool(counter%track_to_detect) or not tracker_ok: 

      # load the input image and construct an input blob for the image
      # by resizing to a fixed 300x300 pixels and then normalizing it
      # (note: normalization is done via the authors of the MobileNet SSD
      # implementation)
      (h, w) = image.shape[:2]
      blob = cv2.dnn.blobFromImage(cv2.resize(image, (300, 300)), 0.007843, (300, 300), 127.5)
      
      # pass the blob through the network and obtain the detections and
      # predictions
      logging.debug('['+str(counter)+']: computing object detections.')
      net.setInput(blob)
      detections = net.forward()
      
      # loop over the detections
      for i in np.arange(0, detections.shape[2]):
        # extract the confidence (i.e., probability) associated with the
        # prediction
        confidence = detections[0, 0, i, 2]
        # filter out weak detections by ensuring the `confidence` is
        # greater than the minimum confidence
        if confidence > args["confidence"]:
          # extract the index of the class label from the `detections`,
          # then compute the (x, y)-coordinates of the bounding box for
          # the object
          idx = int(detections[0, 0, i, 1])
          label = "{}: {:.2f}%".format(CLASSES[idx], confidence * 100)
          logging.info('['+str(counter)+']: ' + label)

          if CLASSES[idx] == 'person':
            box = (detections[0, 0, i, 3:7] * np.array([w-1, h-1, w-1, h-1])).astype("int")
            # obviously they have a different notation.
            bbox = (box[0],box[1], box[2] - box[0], box[3] - box[1])

            tracker = None
            tracker = cv2.TrackerKCF_create()
            tracker_ok = tracker.init(image, bbox)
            logging.info('reinitialized tracker: ' + str(tracker_ok))

    if bbox is not None:
      (startX, startY, endX, endY) = tuple(box)
      #display the prediction
      cv2.rectangle(image, (startX, startY), (endX, endY),COLORS[idx], 2)
      y = startY - 15 if startY - 15 > 15 else startY + 15
      cv2.putText(image, label, (startX, y),cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[idx], 2)

    bbox = None

    msg = 'Tracker status: ' + str(tracker_ok) 
    cv2.putText(image, msg, (3, 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[2], 2)
    #msg2 = 'Tracker status: ' + str(fps.fps()) 
    #cv2.putText(image, msg2, (3, 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[2], 2)

        #out.write(image)
    
    # show the output frame
    cv2.imshow("Frame", image)
    key = cv2.waitKey(1) & 0xFF
 
    # if the `q` key was pressed, break from the loop
    if key == ord("q"):
      break

  # end of main cycle.

  cv2.destroyAllWindows()
  #vs.stop()

  #out.release()
  #vs.release()
  fps.stop()

  logging.info('')
  logging.info('Elasped time: ' + str(fps.elapsed()))
  logging.info('Approx. FPS: '  + str(fps.fps()))

def main():
  
  logging.basicConfig(level=logging.INFO)
  args = parse_options()

  # TODO how to detect this?
  args.update({'input_is_video' : True})

  if args['input_is_video']:
    logging.info('Loading additional packages.')
    # import the necessary packages
    from imutils.video import VideoStream
    from imutils.video import FPS
    import imutils
    import time

  net = load_model(args)
  run(net,args)

if __name__ == '__main__':
  main()
