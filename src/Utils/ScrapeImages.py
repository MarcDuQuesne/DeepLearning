from bs4 import BeautifulSoup
import requests
import re

from urllib.request import urlopen

import os
import argparse
import sys
import json
import logging

screen_width = 1920
screen_height = 1080

URL = "https://www.google.dk" # does changing this make a difference?
header = {
 'User-Agent':"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36"
}

def get_soup(url,header,param):

    s = requests.Session()
    s.headers.update(header)	

    request = s.get(url, params=param)
    return BeautifulSoup(request.text,"html.parser")

    # return BeautifulSoup(urllib2.urlopen(urllib2.Request(url,headers=header)),'html.parser')

def parse_arguments():
    parser = argparse.ArgumentParser(description='Scrape Google images')
    parser.add_argument('-s', '--search', default='bananas', type=str, help='search term')
    parser.add_argument('-n', '--num_images', default=10, type=int, help='num images to save')
    parser.add_argument('-d', '--directory', default='/Users/gene/Downloads/', type=str, help='save directory')
    parser.add_argument('--download', dest='download', action='store_true')
    
    # TODO check consistency of options
    
    return parser.parse_args()

def retrieve_links(args):
    query = args.search 
    max_images = args.num_images
    save_directory = args.directory
    image_type="Action"
    query= query.split()
    query='+'.join(query)
    url=URL+"/search?q="+query+"&source=lnms&tbm=isch"
    
    for start in range(0,max_images,100):
    
        params = {
          "q": query,
          "sa": "X",
          "biw": screen_width,
          "bih": screen_height,
          "tbm": "isch",
          "ijn": start/100,
          "start": start,
          #"ei": "" - This seems like a unique ID, you might want to use it to avoid getting banned. But you probably still are.
        }

        soup = get_soup(url,header,params)
    
        ActualImages=[]# contains the link for Large original images, type of  image
        for a in soup.find_all("div",{"class":"rg_meta"}):
            link , Type =json.loads(a.text)["ou"]  ,json.loads(a.text)["ity"]
            ActualImages.append((link,Type))
            logging.info(link)
            if len(ActualImages) > max_images:
                return ActualImages

    return ActualImages

def DownloadImages(ActualImages):

    for i , (img , Type) in enumerate(ActualImages[:]):
        try:
            req = urllib.request.Request(img, headers={'User-Agent' : header})
            raw_img = urllib.request.urlopen(req).read()
            if len(Type)==0:
                f = open(os.path.join(save_directory , "img" + "_"+ str(i)+".jpg"), 'wb')
            else :
                f = open(os.path.join(save_directory , "img" + "_"+ str(i)+"."+Type), 'wb')
            f.write(raw_img)
            f.close()
        except BaseException as e:
            logging.ERROR('Could not load : '+ str(i))
            #logging.ERROR(e)
            pass


def main(args):

        logging.basicConfig(level=logging.DEBUG)

        args = parse_arguments()
        
        ActualImages = retrieve_links(args)
        if args.download:
            DownloadImages(ActualImages)


if __name__ == '__main__':
    from sys import argv
    try:
        main(argv)
    except KeyboardInterrupt:
        pass
    sys.exit()
