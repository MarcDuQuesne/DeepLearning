import configparser
import logging

class Section(object):
  pass

class Config(object):

    def __init__(self, config_file):

      self.general = Section()
      self.general.stupid_option = True

      self.input = Section()
      self.input.skip      = 0

      self.output = Section()
      self.output.video    = True
      self.output.file     = 'output.avi'
      self.output.format   = '640x480'
      # DIVX, XVID, MJPG, X264, WMV1, WMV2. (XVID is more preferable. 
      # MJPG results in high size video. X264 gives very small size video)
      self.output.codec    = 'XVID' 
      
      self.output.screen   = True
      self.output.timestep = 0.0 # s
      
      self.processing = Section()
      self.processing.confidence = 0.25
      self.processing.edges_method = 'AdaptiveThreshold' # 'Canning' 'Sobel' 'Laplacian'

      parser = configparser.ConfigParser()
      #parser.optionxform = str  # make option names case sensitive
      found = parser.read(config_file)
      if not found:
          raise ValueError('No config file found!')
      for name in parser.sections():
          d = getattr(self,name)
          for n,v in parser[name].items():
            setattr(d,n,v)

      # make boring corrections.
      self.input.skip = int(self.input.skip)
      
      self.output.timestep = float(self.output.timestep)
      w,h = self.output.format.split('x')
      self.output.format   = (int(w),int(h))
      self.processing.confidence = float(self.processing.confidence)

      self.output.video  = parser.getboolean('output', 'video')
      self.output.screen = parser.getboolean('output', 'screen')

      logging.info('Read configuration file '+config_file+'.')

      #for s in self.Sections():
      #  logging.