# import the necessary packages
import threading
import cv2
import logging
import queue

from .Thread import Thread

class WebcamVideoStream(Thread):
  def __init__(self, src=0,usePiCamera=False, resolution=(320, 240), framerate=32):
    super(WebcamVideoStream,self).__init__()

    self.q = queue.Queue(maxsize=100)

    self.name='Input'
    # initialize the video camera stream and read the first frame
    # from the stream
    # check to see if the picamera module should be used
    if usePiCamera:
      # only import the picamera packages unless we are
      # explicity told to do so -- this helps remove the
      # requirement of `picamera[array]` from desktops or
      # laptops that still want to use the `imutils` package
      from pivideostream import PiVideoStream

      # initialize the picamera stream and allow the camera
      # sensor to warmup
      self.stream = PiVideoStream(resolution=resolution,framerate=framerate)

    # otherwise, we are using OpenCV so initialize the webcam
    # stream
    else:
      self.stream = cv2.VideoCapture(src)
  
    self.grab_next_one = threading.Event()
    (self.grabbed, self.frame) = self.stream.read()

    if not self.grabbed:
      logging.error('Cannot capture a frame.')

    # initialize the variable used to indicate if the thread should
    # be stopped
    self.stopped = False

  def run(self):
    # keep looping infinitely until the thread is stopped
    # if the thread indicator variable is set, stop the thread
    while not self.stopped:
      self.q.put(self.stream.read())

  def read(self,skip=0):
    for idx in range(skip):
      self.q.get()
    # return the frame most recently read
    return (self.q.get())
