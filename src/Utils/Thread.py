import logging
import threading
import queue
import numpy as np  

class Thread:
  
  def __init__(self):
    self.q = queue.Queue()
    self.stopped = False
    self.name = ''
    
  def start(self):
    # start the thread to read frames from the video stream
    logging.info('Starting thread ' + self.name)
    self.t = threading.Thread(target=self.run, args=(),name=self.name)
    self.t.start()
    return self

  def run(self):
    while True:
      print('Implement me!')

  def stop(self):
    logging.info(self.name + ' Thread stopped.')
    # indicate that the thread should be stopped
    self.stopped = True
    
    # see the note above.
    if self.q.empty():
      img = np.zeros((10, 10, 3), np.uint8)
      self.q.put(img)

    self.t.join()