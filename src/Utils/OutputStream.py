import threading
import cv2
import time
import logging
import time
import queue

from Utils.Animation.hud import Hud

import numpy as np
from .Thread import Thread

CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
            "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
            "dog", "horse", "motorbike", "person", "pottedplant", "sheep",
            "sofa", "train", "tvmonitor"]

COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))

class OutputStream(Thread):

  def __init__(self,options):
    super(OutputStream,self).__init__()

    self.options = options
    self.actions = []
    self.name='Output'
    self.old_time = time.time()

    if options.output.screen:
      logging.info('Streaming on screen.')
      self.key = None
      self.timestep = options.output.timestep
      self.actions.append(self.show)

    if options.output.video:
      logging.info('Creating video file '+options.output.file+'.' )
      # Define the codec and create VideoWriter object
      self.fourcc    = cv2.VideoWriter_fourcc(*options.output.codec)
      self.outstream = cv2.VideoWriter(options.output.file,self.fourcc, 20.0, options.output.format)
      self.actions.append(self.record)

  def initialize(self,shape):
    color = (0,0,0)
    self.hud = Hud(shape,color=color)

  def show(self,img):
    self.wait_at_least(self.timestep)
    cv2.imshow('output',img)
    self.key = cv2.waitKey(1)
    
  def record(self,img):
    self.outstream.write(img)

  def wait_at_least(self,timestep):
      new_time = time.time()
      # is enough time passed?
      if self.timestep - (new_time - self.old_time) > 0 :
        time.sleep(timestep - (new_time - self.old_time) )
      self.old_time = new_time

  def put_overlay(self,img,detections):
    
    (h, w) = img.shape[:2]
    # loop over the detections
    for i in np.arange(0, detections.shape[2]):
      # extract the confidence (i.e., probability) associated with the
      # prediction
      confidence = detections[0, 0, i, 2]
      # filter out weak detections by ensuring the `confidence` is
      # greater than the minimum confidence
      if confidence > self.options.processing.confidence:
        # extract the index of the class label from the `detections`,
        # then compute the (x, y)-coordinates of the bounding box for
        # the object
        idx = int(detections[0, 0, i, 1])
        box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
        (startX, startY, endX, endY) = box.astype("int")
        #display the prediction
        label = "{}: {:.2f}%".format(CLASSES[idx], confidence * 100)
        logging.debug(label)
        cv2.rectangle(img, (startX, startY), (endX, endY),COLORS[idx], 2)
        y = startY - 15 if startY - 15 > 15 else startY + 15
        cv2.putText(img, label, (startX, y),cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[idx], 2)

    # just a test, remove as soon as it's ready.
    #if counter == 50:
    #  self.hud.RM.TW.reinitialize('SYSTEM WARNING',blink=False)
    #  self.hud.Stream.stream('Detected Problem...')
    #elif counter == 80:
    #  self.hud.RM.TW.reinitialize('SYSTEM CRITICAL',blink=True)
    #  self.hud.Stream.stream('Detected Solution...')
    #elif counter == 100:
    #  self.hud.Stream.stream('Again Text...')

  def run(self):
    # keep looping infinitely until the thread is stopped
    while not self.stopped:
      # get an image from the queue
      img,detection = self.q.get(block=True)
      logging.debug('Received New Image.')

      # EDGE detection. 
      #logging.info('Processing new Image')
      #edges = Edges(img,method='Canny')
      #edges = Edges(img,method=self.options.processing.edges_method)
      #kernel = np.ones((3,3),np.uint8)
      #edges = cv2.morphologyEx(edges, cv2.MORPH_OPEN, kernel)
      #edges = cv2.morphologyEx(edges, cv2.MORPH_CLOSE, kernel)
      #edges = cv2.cvtColor(edges, cv2.COLOR_GRAY2BGR)

      #edges = Edges(edges,method='Canny')
      
      #coeff = 0.5
      #cv2.addWeighted(edges, coeff, img, 1 - coeff,0, edges)

      # object detection..
      #self.hud.run(edges)

      self.put_overlay(img,detection)

      for action in self.actions:
        action(img)
        
  def stop(self):
    logging.info(self.name + ' Thread stopped.')
    # indicate that the thread should be stopped
    self.stopped = True
    
    # see the note above.
    if self.q.empty():
      img = np.zeros((10, 10, 3), np.uint8)
      detections = np.zeros((1, 1, 1, 3), np.uint8)
      self.q.put((img,detections))

    self.t.join()