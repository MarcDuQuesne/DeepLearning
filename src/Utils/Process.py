import logging
import queue
import threading
import numpy as np
import cv2

from Utils.Animation.draw    import create_blank
from Utils.Animation.ImageUtils import Edges
from .Thread import Thread

model     = './Models/MobileNetSSD_deploy.caffemodel'
prototxt  = './Models/MobileNetSSD_deploy.prototxt.txt'

class Process_Image(Thread):
  def __init__(self,output,options):
    super(Process_Image,self).__init__()
    
    self.options = options
    self.output = output
    self.net = cv2.dnn.readNetFromCaffe(prototxt, model)

    self.name = 'Process.'


  def run(self):
    
    # keep looping infinitely until the thread is stopped
    counter=0
    while not self.stopped:
      counter+=1
      # get an image from the queue
      img = self.q.get(block=True)
      logging.debug('Processing image.')

      # load the input image and construct an input blob for the image
      # by resizing to a fixed 300x300 pixels and then normalizing it
      # (note: normalization is done via the authors of the MobileNet SSD
      # implementation)
      blob = cv2.dnn.blobFromImage(cv2.resize(img, (300, 300)), 0.007843, (300, 300), 127.5)

      # pass the blob through the network and obtain the detections and
      # predictions
      logging.debug('['+str(counter)+']: computing object detections.')
      self.net.setInput(blob)
      detections = self.net.forward()

      self.output.q.put((img,detections))
