import cv2
import numpy as np

from .widget import Widget
from .draw import create_blank

class SquareGrid():

  def __init__(self,shape,stride):
    Widget.__init__(self)
    
    self.width,self.height,_ = shape
    self.stride = stride

    self.grid = create_blank(self.height,self.width,color=(255,255,255))

    for idx in  np.arange(0,self.height+self.stride,self.stride):
      cv2.line(self.grid,
              (idx,0),
              (idx,self.height),
              (0,0,0))

    for idy in np.arange(0,self.width+self.stride,self.stride):
      cv2.line(self.grid,
              (0,idy),
              (self.height,idy),
              (0,0,0))

  def run(self,img):
    coeff = 0.1

    #output = create_blank(self.height,self.width,color=(255,255,255))
    cv2.addWeighted(self.grid, coeff, img, 1 - coeff,0, img)