import cv2
import numpy as np

from .draw import draw_circle_with_gaps
from .widget import Widget

class Sight(Widget):
  
  def __init__(self,center=(0,0),radius=10,thickness=1,color=(0,0,0)):
    Widget.__init__(self)

    self.color = color
    self.thickness = thickness
    self._counter = 0
    self.center = center
    self.radius = radius

    deg_delta = 10.
    delta = 2*np.pi * deg_delta/360
    self.period = 360./deg_delta

    # TODO size should be the period..
    counter = np.arange(0,2*np.pi,delta)
    self.angle = np.pi*np.sin(counter) # + np.cos(2*np.pi/500*counter)

  def run(self,img):
    
    self._counter = int((self._counter + 1) % self.period)
    angle = self.angle[self._counter]

    draw_circle_with_gaps(img,center=self.center,radius=self.radius,ratio=0.7,sectors=3,width=6,angle=angle)
    cv2.circle(img,center=self.center,radius=self.radius,color=self.color)
  
    #draw_circle_with_gaps(img,center=center,radius=int(radius[2]),ratio=0.5,angle=angle2,sectors=3,color=color)
    #cv2.circle(img,center=center,radius=int(radius[2]),color=color)
