class Widget():
  def __init__(self):
    self.show=True

  def toggle(self):
    if self.show == True:
      self.show = False
    elif self.show == False:
      self.show = True

  def run(self,img):
    if self.show == False:
      return
    pass

class WidgetGroup():
  
  def __init__(self):
    self.show=True
    self.widgets = []

  def toggle(self):
    for widget in self.widgets:
      widget.toggle()

  def run(self,img):
    if self.show == False:
      return
    for widget in self.widgets:
      widget.run(img)
