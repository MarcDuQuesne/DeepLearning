import cv2
import numpy as np
from math import ceil,floor

lookup_table = {}

def create_blank(width, height, color=(0, 0, 0)):
    # Create black blank image
    image = np.zeros((height, width, 3), np.uint8)

    # Since OpenCV uses BGR, convert the color first
    color = tuple(reversed(color))
    # Fill image with color
    image[:] = color

    return image

def update_lookup_table(radius,ratio,sectors):
  
  n_t = 60
  #n_t = int(ceil(n_t / sectors) * sectors)

  n_c   = int( (ratio     * n_t / sectors))
  n_nnc = int( ((1-ratio) * n_t / sectors))

  # how to find the optimal value?
  d_alpha = 2*np.pi/n_t

  p = []
  for angle in np.arange(0,2*np.pi,d_alpha):
    p.append((int(radius * np.cos(angle)),
              int(radius * np.sin(angle))))
  
  indices = np.array([])

  for i in range(sectors):
    indices = np.concatenate((indices,np.ones ( n_c   )))
    indices = np.concatenate((indices,np.zeros( n_nnc )))

  if n_t > len(indices):
    indices = np.concatenate((indices,np.zeros( n_t - len(indices) )))
  
  indices = indices.astype(bool)

  lookup_table.update({ (radius,sectors) : (d_alpha,p,indices) })

def draw_circle_with_gaps(img,center,radius,ratio,angle=0,width=1,sectors=7,color=(0,0,0)):

  c_x,c_y = center

  if not (radius,sectors) in lookup_table:
    update_lookup_table(radius,ratio,sectors)

  d_alpha, points, indices  = lookup_table[ (radius,sectors) ]

  i_0 = int(angle / d_alpha)

  # points in the right order
  _points  = np.roll(points, 0)
  _indices = np.roll(indices,  i_0)

  for idx in range(len(_points)-1):
    
    if _indices[idx] and _indices[idx+1]:
      cv2.line(img,
             tuple(_points[idx]   + [c_x,c_y]),
             tuple(_points[idx+1] + [c_x,c_y]),
             color,width)
  
  # return _points

def draw_lines(img,center,radius,angle=0,sectors=4,color=(0,0,0)):
  
  c_x,c_y = center

  for alpha in np.arange(angle,2*np.pi + angle,2*np.pi/sectors):

      p1 = (int(c_x + radius[0] * np.cos(alpha)),
            int(c_y + radius[0] * np.sin(alpha)))
      p2 = (int(c_x + radius[1] * np.cos(alpha)),
            int(c_y + radius[1] * np.sin(alpha)))

      cv2.line(img,p1,p2,color)

def square_sight(img,center,size,color):
      
      px,py = center
      
      p1 = (int(px + 0.25 * size),
            int(py + 0.5  * size))

      p2 = (int(px + 0.5 * size),
            int(py + 0.5 * size))
      
      p3 = (int(px - 0.25 * size),
            int(py + 0.5 * size))

      cv2.line(img,p1,p2,color)
  

