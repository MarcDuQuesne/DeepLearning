import os
import cv2
import multiprocessing

from .widget import Widget,WidgetGroup
from .terminal import Terminal
from .textstream import TextStream
from .loadbar import LoadBar
from .revloadbar import RevLoadBar
from .sight import Sight
from .grid import SquareGrid

def load_average():
  return os.getloadavg()[0]

def cpuinfo():
  return multiprocessing.cpu_count()

def meminfo():
  tot_m, used_m, free_m = map(int, os.popen('free -m').readlines()[-1].split()[1:])
  return tot_m, used_m, free_m

def occupied_memory():
  tot_m, used_m, free_m = meminfo()
  return float(used_m)

def temp():
    #return float(os.popen('/opt/vc/bin/vcgencmd measure_temp').readlines()[0].split('=')[1][0:4])
    return 36.7 #float(os.popen('/opt/vc/bin/vcgencmd measure_temp').readlines()[0].split('=')[1][0:4])

# RESOURCES MONITORING SYSYEM.
class Resources_Monitor(WidgetGroup):
  
  def __init__(self,shape,color=(0,0,0)):
    WidgetGroup.__init__(self)
    
    n_cores = cpuinfo()
    ram, _, _ = meminfo()

    self.TW  = Terminal('  SYSTEM OK',
                        (int(0.01*shape[1]), int(0.95*shape[0])),
                        blink=False,
                        color=color)
    self.MemoryBar = LoadBar(shape=shape,
                              function=occupied_memory,
                              heightf=[0.75,0.9],
                              widthf=[0.075,0.9],
                              nticks=3,
                              limits=[0,ram],
                              title='RAM',
                              color=color)
    self.CPUBar = RevLoadBar(shape=shape,
                                function=load_average,
                                heightf=[0.5,0.9],
                                widthf=[0.05,0.9],
                                nticks=5,
                                limits=[0,n_cores],
                                title='CPU',
                                color=color)

    self.TempBar = RevLoadBar(shape=shape,
                                function=temp,
                                limits=[0,70],
                                heightf=[0.5,0.9],
                                widthf=[0.95,0.9],
                                nticks=5,
                                title='T',
                                color=color)
                                
    self.widgets = [self.TW,self.CPUBar,self.MemoryBar,self.TempBar]


# MAIN CLASS
class Hud(WidgetGroup):

  def __init__(self,shape,color=(0,0,0)):
    WidgetGroup.__init__(self)
    
    self.shape = shape

    self.RM = Resources_Monitor(shape,color=color)
    self.Stream  = TextStream('Initializing systems...',
                        (int(0.3*shape[1]), int(0.90*shape[0])),
                        cursor=True,
                        color=color)

#    self.Grid    = SquareGrid(shape=shape,
#                              stride=50)
#    self.View    = Sight(center=(0,0),
#                         radius=1)
    self.widgets = [ self.RM,self.Stream ]#],self.Grid] #self.View]

