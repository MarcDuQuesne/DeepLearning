import cv2
import numpy as np

from .widget import Widget
from .terminal import Terminal

class RevLoadBar(Widget):
  
  def __init__(self,shape,function,thickness=2,color=(0,0,0), heightf=[0.1,0.9],widthf=[0.1,0.9],nticks=5,limits=[0,1],title=''):
    Widget.__init__(self)

    self.color = color
    self.thickness = thickness
    self.function = function
    self.fontScale = 1
    self.fontFace = cv2.FONT_HERSHEY_PLAIN
    self.nticks = nticks
    (height,width,_) = shape

    self.title = Terminal(title,(int((widthf[0]-0.01)*width),int((heightf[0] - 0.01 )*height)))
    self.title.counter = len(title)+1

    self.pt1 = ( int(widthf[0]*width), int(heightf[0]*height))
    self.pt2 = ( int(widthf[0]*width), int(heightf[1]*height))

    self.range=limits
    self.epsilon = float(self.range[1] - self.range[0])/self.nticks

    self.height, self.width = height,width

    self.delta = int((self.pt2[1]-self.pt1[1]) / nticks)
    self.pt2 =  ( int(widthf[0]*width), self.pt1[1] + nticks*self.delta)


    self.pt3 = (int(self.pt1[0] + 0.005*width), self.pt1[1] + int(0.80*(self.pt2[1]-self.pt1[1])))
    self.pt4 = (int(self.pt1[0] + 0.005*width), self.pt2[1])

    px = self.pt1[0]
    self.p=[]
    max_l = max(len(str(int(limits[0]))),len(str(int(limits[1]))))
    for py in range(self.pt1[1],self.pt2[1]+self.delta,self.delta):
      self.p.append( (( px , py),
                     ( int(px-0.01*self.width), py),
                     ( int(px-0.025*self.width - 10*max_l), py+5)))

  def run(self,img):
    cv2.line(img, self.pt1, self.pt2, self.color, self.thickness)
    cv2.line(img, self.pt3, self.pt4, self.color, 2*self.thickness)
    
    cntr=0
    for pt1,pt2,pt3 in self.p:
      cv2.line(img, pt1, pt2, self.color, self.thickness)
      cv2.putText(img  = img,
                text = '{:.1f}'.format(cntr*self.epsilon-self.range[0]),
                org  =    pt3,
                fontFace  = self.fontFace,
                fontScale = self.fontScale,
                thickness = self.thickness,
                color     = self.color)
      cntr+=1


    def _triangle(center):
      x1,y1 = (int(center[0] + 0.005*self.width), int(center[1] - 0.005*self.width))
      x2,y2 = center 
      x3,y3 = (int(center[0] + 0.005*self.width), int(center[1] + 0.005*self.width))

      return np.array([[x1,y1],[x2,y2],[x3,y3]])

    he = self.pt1[1] + int( self.function() * (self.pt2[1] - self.pt1[1]) / (self.range[1] -self.range[0]))
    up = (self.pt1[0]+int(0.005*self.width),he)
    #cv2.fillConvexPoly(img, _triangle(self.pt1), self.color)
    cv2.fillPoly(img, [_triangle(up)], self.color)

    self.title.run(img)
