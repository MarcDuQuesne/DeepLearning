import cv2
import numpy as np
import time

from .terminal import Terminal
from .widget import Widget

class TextStream(Widget):
  
  #counter = 0

  def __init__(self,text,position,
               cursor=None,
               fontScale = 1,fontFace = cv2.FONT_HERSHEY_PLAIN,thickness=2,color=(0,0,0),
               max_carachters=100,
               max_lines=3):
    Widget.__init__(self)

    px,py = position

    self.text=''
    self.stream(text)
    self.Tlines = []
    self.max_carachters = max_carachters
    self.cursor=cursor

    for l in range(max_lines):
      py = position[1] - 20*l
      self.Tlines.append(Terminal('',
                            (px,py),
                            fontScale,fontFace,thickness,
                            blink=None,
                            cursor=None,
                            color=color))

    self.Tlines[0].reinitialize(self.text[:self.max_carachters],cursor=self.cursor)
    self.text = self.text[self.max_carachters:]

  def stream(self,text):
    self.text += '[' + time.strftime("%H:%M:%S") + '] ' +text
    self.counter = 0

  def run(self,img):
  
    if len(self.text) > 0:
        l1 = self.Tlines[0]
        t = l1.text
        if (len(t) == l1.counter):
          for idx in range(0,len(self.Tlines)):
           if(idx+1 < len(self.Tlines)):
             l2 = self.Tlines[idx+1]
             w = l2.text
             l2.reinitialize(text=t)
             l2.counter = len(l2.text) + 1
             t=w
          l1.reinitialize(self.text[:self.max_carachters],cursor=self.cursor)
          self.text = self.text[self.max_carachters:]

    for l in self.Tlines:
      l.run(img)
