import cv2

from .widget import Widget

class Terminal(Widget):

  counter = 0

  def __init__(self,text,position,
               fontScale = 1,fontFace = cv2.FONT_HERSHEY_PLAIN,thickness=2,color=(0,0,0),
               blink=None,cursor=None):
    Widget.__init__(self)

    self.text      = text
    self.position  = position
    self.fontFace  = fontFace
    self.fontScale = fontScale
    self.thickness = thickness 
    self.color     = color 

    self.blink = blink
    self.cursor = cursor
    self.counter = 0

    self.bcounter = 0
    
  def reinitialize(self,text,blink=None,cursor=None):
    self.text = text
    self.counter = 0
    self.blink = blink
    self.cursor = cursor

  def run(self,img):
    
    if self.counter < len(self.text):
      _text = self.text[:self.counter]

      self.counter+=1
    else:      
      _text = self.text
    
    if self.cursor == True:
      _text+='|'
      self.cursor = False  
    elif self.cursor == False:
      self.cursor = True  

    if self.blink == True:
      _text = ''
      self.blink = False
      self.bcounter+=1
    elif self.blink == False and bool(self.bcounter):
      self.blink = True
      self.bcounter=0

    cv2.putText(img  = img,
                text = _text,
                org  = self.position,
                fontFace  = self.fontFace,
                fontScale = self.fontScale,
                thickness = self.thickness,
                color     = self.color)
