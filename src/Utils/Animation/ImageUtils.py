import numpy as np
import cv2

import logging

def getSobel (channel):

    sobelx = cv2.Sobel(channel, cv2.CV_16S, 1, 0, borderType=cv2.BORDER_REPLICATE)
    sobely = cv2.Sobel(channel, cv2.CV_16S, 0, 1, borderType=cv2.BORDER_REPLICATE)
    sobel = np.hypot(sobelx, sobely)

    return sobel;

def Edges(img,method='Sobel'):

  if method == 'Sobel':
    blurred = cv2.GaussianBlur(img, (3, 3), 0)
    edges = np.max( np.array([getSobel(blurred[:,:, 0]),
                              getSobel(blurred[:,:, 1]),
                              getSobel(blurred[:,:, 2]) ]),
                             axis=0 )
  elif method == 'Canny':
    sigma = 0.33
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (3, 3), 0)
    # compute the median of the single channel pixel intensities
    v = np.median(blurred)

    # apply automatic Canny edge detection using the computed median
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))
    edges = cv2.Canny(blurred,lower,upper)
    edges = cv2.cvtColor(edges, cv2.COLOR_GRAY2BGR)

  elif method == 'AdaptiveThreshold':
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (3, 3), 0)
    edges = cv2.adaptiveThreshold(blurred,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,11,2)
    edges = cv2.cvtColor(edges, cv2.COLOR_GRAY2BGR)

  elif method == 'Laplacian':
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (3, 3), 0)
    edges = cv2.Laplacian(blurred,cv2.CV_64F)
    
    #ret,edges = cv2.threshold(lap,5,255,cv2.THRESH_BINARY)
    edges = np.uint8(edges)
    edges = cv2.cvtColor(edges, cv2.COLOR_GRAY2BGR)
    

  else:
    logging.error(str(method) + ' is not a valid edge method.')

  return edges
