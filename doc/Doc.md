
# python sockets 
https://steelkiwi.com/blog/working-tcp-sockets/

# general resources, projects
https://github.com/pjreddie/TopDeepLearning

# deep learning and opencv
https://www.pyimagesearch.com/2017/08/21/deep-learning-with-opencv/

# face recognition
http://allskyee.blogspot.nl/2017/03/face-detection-and-recognition-using.html
https://github.com/davidsandberg/facenet
https://github.com/elucideye/drishti // eye movement

# opencv dnn examples
https://github.com/opencv/opencv/tree/master/samples/dnn

# source of models/weights
https://pjreddie.com/darknet/yolo/

# tensorflow models
https://github.com/tensorflow/models

# repo with utils to work with imagenet
https://github.com/tzutalin/ImageNet_Utils

# retrain a model, both vgg and the like and mobilenet.
https://hackernoon.com/creating-insanely-fast-image-classifiers-with-mobilenet-in-tensorflow-f030ce0a2991
