#!/bin/bash

pylint --rcfile=.pylintrc --output-format=text ./*/*.py | tee public/pylint.txt 

score=$(sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' public/pylint.txt)
echo "Pylint score was $score" | tee public/result.txt
anybadge --label pylint --value=$score --file=public/pylint.svg pylint

exit 0
